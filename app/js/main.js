var ui_comparativo = (function( $ ) {
	'use strict';

	var init = function(){
			// EXIBE APENAS A TELA DA HOME
			$('.main').eq(0).show();
		},

		nav = function(){
			$(".btn-nav").click(function(){
			    var cls = $(this).attr('id')
			    $(".content").fadeOut(500);
			    $('.' + cls).delay(500).fadeIn(1000);
			    return false;
			})
		},

		modalHome = function(){
			var currentModal;

			$('.btn-group-home').find('a').on('click', function(){
				currentModal = $($(this).attr('href'));

				currentModal.fadeIn();
				
				//console.log($(that.attr('href')).is(':visible'));

			});

			$('.close').on('click', function(){
				currentModal.fadeOut();
			})
		},

		ready = function() {
			init();
			nav();
			modalHome();
		};

	return {
		ready: ready
	};

})( jQuery );


jQuery(ui_comparativo.ready);
